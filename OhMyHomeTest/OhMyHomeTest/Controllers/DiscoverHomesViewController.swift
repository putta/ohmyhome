//
//  DiscoverHomesViewController.swift
//  OhMyHomeTest
//
//  Created by Chandra on 24/02/18.
//  Copyright © 2018 Chandra. All rights reserved.
//

import UIKit
import AsyncDisplayKit

class DiscoverHomesViewController: UIViewController{
    
    let cellIdentifier = "CellIdentifier"
    let discoverTableView = UITableView()
    var discoverHomesViewModel : DiscoverHomesViewModel!
    var homeDataSourceArray : [HomeDataSource] = []
    var viewsDictionary :[Int:HorizontalSliderView] = [:]
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        discoverHomesViewModel = DiscoverHomesViewModel(delegate: self)
        addTableView()
        addSearchView()
        activityIndicator()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = Strings.ohMyHome
        self.navigationController?.navigationBar.barTintColor = UIColor.navigationBarColor()
        self.navigationController?.navigationBar.isTranslucent = false
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        let backItem = UIBarButtonItem()
        backItem.title = Strings.back
        navigationItem.backBarButtonItem = backItem
    }
    
    func addTableView() {
        discoverTableView.backgroundColor = UIColor.white
        discoverTableView.register(UITableViewCell.self, forCellReuseIdentifier:cellIdentifier)
        discoverTableView.delegate = self
        discoverTableView.dataSource = self
        discoverTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.view.addSubview(discoverTableView)
        discoverTableView.allowsSelection = false
        discoverTableView.reloadData()
    }
    
    func addSearchView() {
        let searchView = SearchView()
        searchView.searchBar.delegate = self
        self.view.addSubview(searchView)
        self.view.addContraintsWithFormat("H:|[v0]|", views: searchView)
        self.view.addContraintsWithFormat("V:|[v0(\(UIConstants.searchBarHeight))]", views: searchView)
        self.view.addContraintsWithFormat("H:|[v0]|", views: discoverTableView)
        self.view.addContraintsWithFormat("V:|-\(UIConstants.searchBarHeight)-[v0]|", views: discoverTableView)
    }
    
    func activityIndicator(){
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        myActivityIndicator.frame.size.height = 44.0
        myActivityIndicator.center = view.center
        myActivityIndicator.startAnimating()
        self.discoverTableView.tableHeaderView = myActivityIndicator
    }
}

//MARK: DiscoverHomesViewModelDelegate Methods
extension DiscoverHomesViewController: DiscoverHomesViewModelDelegate {
    func updateViewContentSignal(data: Any?) {
        if let array = data as? [HomeDataSource]
        {
            self.homeDataSourceArray = array
            discoverTableView.beginUpdates()
            discoverTableView.insertRows(at: [IndexPath(row: homeDataSourceArray.count-1, section: 0)], with: .automatic)
            discoverTableView.endUpdates()
        }
    }
    
    func bannersUpdated(banners: [Banner]) {
        let bannersView = BannerView(frame:UIConstants.bannerViewFrame, dataSource : banners)
        bannersView.delegate = self
        discoverTableView.tableHeaderView = bannersView
    }
    
    func specialBannerButtonTapped(view: SpecialBannerCellNode) {
        let vc = UIViewController()
        vc.hidesBottomBarWhenPushed = true
        
        switch view.specialBannerType {
        case .postMyProperty:
            vc.view.backgroundColor = .orange
        case .loanCalculator:
            vc.view.backgroundColor = .yellow
        case .openMap:
            vc.view.backgroundColor = .gray
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: SearchBarDelegate Methods
extension DiscoverHomesViewController: UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        //Redirect to search detail page
        let vc = UIViewController()
        vc.view.backgroundColor = .red
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
        return false
    }
}

//MARK: BannerViewDelegate Methods
extension DiscoverHomesViewController: BannerViewDelegate {
    func bannerView(view: BannerView, didSelectAt index: Int) {
        guard let url = URL(string:(self.discoverHomesViewModel.bannerAtIndex(index: index)?.urlLink)!) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}

//MARK: HorizontalSliderViewDelegate Methods
extension DiscoverHomesViewController: HorizontalSliderViewDelegate {
    func horizontalSliderView(view: HorizontalSliderView, didSelectAt index: Int) {
        let vc = UIViewController()
        vc.view.backgroundColor = .blue
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func horizontalSliderView(view: HorizontalSliderView, viewMore sender: UIButton) {
        let vc = UIViewController()
        vc.view.backgroundColor = .green
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: UITableViewDelegate,UITableViewDataSource Methods
extension DiscoverHomesViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return DiscoverCellLayout.CellHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homeDataSourceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath as IndexPath)
        
        if let view = cell.viewWithTag(201)
        {
            view.removeFromSuperview()
        }
        if let view = viewsDictionary[indexPath.row]
        {
            cell.addSubview(view)
        }
        else
        {
            let homesHorizontalView = HorizontalSliderView(frame:DiscoverCellLayout.CardFrame, cellType:CellType(rawValue: indexPath.row % 2)!)
            homesHorizontalView.delegate = self
            homesHorizontalView.tag = 201 // for ideintification purpose
            cell.addSubview(homesHorizontalView)
            
            homesHorizontalView.loadbleCellType = CellType(rawValue: indexPath.row % 2)!
            let dataSource = self.homeDataSourceArray[indexPath.row]
            homesHorizontalView.dataSource = dataSource
            homesHorizontalView.categoryName = dataSource.displayName
            viewsDictionary[indexPath.row] = homesHorizontalView
        }
        return cell
    }
}


