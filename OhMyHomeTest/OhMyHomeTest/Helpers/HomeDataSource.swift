//
//  HomeDataSource.swift
//  OhMyHomeTest
//
//  Created by Chandra on 02/03/18.
//  Copyright © 2018 Chandra. All rights reserved.
//

import Foundation

struct HomeDataSource {
    var displayName : String! = nil
    var cardsArray : [Any]
    var cardType : CardsType
    
    init(cardsArray:[Any], cardType:CardsType) {
        self.cardsArray = cardsArray
        self.cardType = cardType
        self.displayName = getDaisplayName()
    }
}

extension HomeDataSource{
    func getDaisplayName() -> String {
        
        switch self.cardType {
        case .featured:
            return "Featured Listings".uppercased()
        case .latest:
            return "Latest Listings".uppercased()
        case .openHouse:
            return "Open House".uppercased()
        case .under:
            return "Listings Under $300,000".uppercased()
        case .houseRent:
            return "Latest Flat Rental".uppercased()
        case .roomRent:
            return " Latest Rooms For Rent".uppercased()
        }
    }
}

enum CardsType : String{
    case featured   = "featured"
    case latest     = "latest"
    case openHouse  = "openHouse"
    case under      = "under"
    case houseRent  = "houseRent"
    case roomRent   = "roomRent"
    
    static let allValues  = [featured, latest, openHouse, under, houseRent, roomRent]
}
