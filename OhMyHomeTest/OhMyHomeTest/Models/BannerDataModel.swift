//
//  BannerDataModel.swift
//  OhMyHomeTest
//
//  Created by Chandra on 28/02/18.
//  Copyright © 2018 Chandra. All rights reserved.
//

import UIKit

class BannerDataModel: NSObject {
    let banners : [Banner]
    init?(dictionary : JSONDictionary)
    {
        guard let data : JSONDictionary = dictionary["_data"] as? JSONDictionary, let bannersArray = data["banners"] as? [JSONDictionary] else { return nil}
        self.banners =  bannersArray.flatMap(Banner.init)
    }
}

