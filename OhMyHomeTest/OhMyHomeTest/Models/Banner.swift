//
//  Banner.swift
//  OhMyHomeTest
//
//  Created by Chandra on 26/02/18.
//  Copyright © 2018 Chandra. All rights reserved.
//

import UIKit

typealias JSONDictionary = [String : Any]
typealias JSONArray = [JSONDictionary]

struct Banner {
    let _id : String
    let imageName : String
    let urlLink : String
    
    init?(dictionary:JSONDictionary) {
        guard let _id = dictionary["_id"] as? String else { print("error parsing JSON within PhotoModel Init6"); return nil}
        self._id = _id
        self.imageName = dictionary["imageName"] as? String ?? ""
        self.urlLink = dictionary["urlLink"] as? String ?? ""
    }
}


