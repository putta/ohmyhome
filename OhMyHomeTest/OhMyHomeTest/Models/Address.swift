//
//  Address.swift
//  OhMyHomeTest
//
//  Created by Chandra on 28/02/18.
//  Copyright © 2018 Chandra. All rights reserved.
//

import UIKit

struct Address{
    var block : String
    var road : String
    
    init?(dictionary:JSONDictionary) {
        self.block = dictionary["block"] as? String ?? ""
        self.road = dictionary["road"] as? String ?? ""
    }
}

