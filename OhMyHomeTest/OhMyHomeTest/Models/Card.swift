//
//  Card.swift
//  OhMyHomeTest
//
//  Created by Chandra on 28/02/18.
//  Copyright © 2018 Chandra. All rights reserved.
//

import UIKit

struct Card {
    let _id           : String
    let imageName     : String
    let price         : NSNumber
    let numberOfBeds  : NSNumber
    let numberOfToilets : NSNumber
    let sizeSQM       : NSNumber
    let address       : Address?
    
    init?(dictionary:JSONDictionary) {
        guard let _id = dictionary["_id"] as? String else { print("error parsing JSON within PhotoModel Init6"); return nil}
        self._id = _id
        self.imageName = dictionary["imageName"] as? String ?? ""
        self.price = dictionary["price"] as? NSNumber ?? 00 as NSNumber
        self.numberOfBeds = dictionary["numberOfBeds"] as? NSNumber ?? 00 as NSNumber
        self.numberOfToilets = dictionary["numberOfToilets"] as? NSNumber ?? 00 as NSNumber
        self.sizeSQM = dictionary["sizeSQM"] as? NSNumber ?? 00 as NSNumber
        if let addressDict = dictionary["address"] as? JSONDictionary
        {
            self.address = Address(dictionary:addressDict)!
        }
        else
        {
            self.address = nil
        }
    }
}

extension Card
{
    func attributedPriceString() -> NSAttributedString {
        return ("$"+String(describing: self.price)).attributedString(color: .black, font: Fonts.priceFont)
    }
    
    func attributedBedsCountString() -> NSAttributedString {
        return ("\u{262F}"+String(describing: self.numberOfBeds)).attributedString(color: Colors.numberOfBedsTextColor, font: Fonts.numberOfBedsFont)
    }
    
    func attributedToiletsCountString() -> NSAttributedString {
        return ("\u{262F}"+String(describing: self.numberOfToilets)).attributedString(color: Colors.numberOfToiletsTextColor, font: Fonts.numberOfToiletsFont)
    }
    
    func attributedSizeSQMString() -> NSAttributedString {
        return ("\u{262F}"+String(describing: self.sizeSQM)+" sqm").attributedString(color: Colors.sizeSQMTextColor, font: Fonts.sizeSQMFont)
    }
    
    func attributedAddressString() -> NSAttributedString {
        if let address = self.address
        {
            return (address.block+", "+address.road).attributedString(color: Colors.addressTextColor, font: Fonts.addressFont)
        }
        return NSAttributedString(string:"")
    }
}

