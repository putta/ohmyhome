//
//  CardsDataModel.swift
//  OhMyHomeTest
//
//  Created by Chandra on 28/02/18.
//  Copyright © 2018 Chandra. All rights reserved.
//

import UIKit

class CardsDataModel: NSObject {
    let cards : [Card]
    init?(dictionary : JSONDictionary)
    {
        guard let data : JSONDictionary = dictionary["_data"] as? JSONDictionary, let bannersArray = data["cards"] as? [JSONDictionary] else { return nil}
        self.cards =  bannersArray.flatMap(Card.init)
    }
}

