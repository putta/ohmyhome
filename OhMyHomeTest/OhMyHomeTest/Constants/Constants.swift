//
//  Constants.swift
//  OhMyHomeTest
//
//  Created by Chandra on 25/02/18.
//  Copyright © 2018 Chandra. All rights reserved.
//

import UIKit

let screenSize = UIScreen.main.bounds.size

struct URLS {
    static let ApiKey = "eRpdxrvyuYibDa1CfQgG1svcejbr2Lp1OPRUKaD1"
    static let Host = "https://staging.omh.sg/itv/_discovery/"
    static let BannersEndPoint = "banners"
    static let CardsEndPoint = "cards?name="
    static let ImageEndPoint = "image?name="
}

extension URLS{
    static let BannersUrl = Host + BannersEndPoint
    static let CardsUrl   = Host + CardsEndPoint
    static let ImageUrl   = Host + ImageEndPoint
}

struct DiscoverCellLayout {
    static let CellHeight            : CGFloat = 300.0
    static let HorizontalBuffer      : CGFloat = 10.0
    static let TopBuffer             : CGFloat = 5.0
    static let spacingFooterItems    : CGFloat = 5.0
    static let CardHeaderHeight      : CGFloat = 34.0
    static let HorizontalCardPadding : CGFloat = 25.0
    
    static let flowLayoutSectionInsets = UIEdgeInsetsMake(0, 5, 0, 5)
    static let CardFrame : CGRect = CGRect(x: 0, y: 0.0, width: screenSize.width, height: CellHeight - 10)
    static let InsetForPhoto : UIEdgeInsets = UIEdgeInsets(top: TopBuffer, left: HorizontalBuffer, bottom: TopBuffer, right: 0)
    static let InsetZero : UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    static let InsetForFollowButton : UIEdgeInsets = UIEdgeInsets(top: 0, left: CGFloat.infinity, bottom: CGFloat.infinity, right: 0)
    static let InsetForBotomCenteredButton : UIEdgeInsets = UIEdgeInsets(top: CGFloat.infinity, left: CGFloat.infinity, bottom: 12, right: CGFloat.infinity)
}

struct BannerCellLayout {
    static let InsetForBannerImage : UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom:10, right: 0)
}

struct Fonts {
    static let numberOfBedsFont    : UIFont = .systemFont(ofSize: 14.0)
    static let numberOfToiletsFont : UIFont = .systemFont(ofSize: 14.0)
    static let sizeSQMFont         : UIFont = .systemFont(ofSize: 14.0)
    static let priceFont           : UIFont = .systemFont(ofSize: 20.0)
    static let addressFont         : UIFont = .boldSystemFont(ofSize: 16.0)
    
    static let categoryTitleFont   : UIFont = .boldSystemFont(ofSize: 16.0)
    static let viewMoreFont        : UIFont = .boldSystemFont(ofSize: 16.0)
}

struct Colors {
    static let numberOfBedsTextColor    : UIColor = .gray
    static let numberOfToiletsTextColor : UIColor = .gray
    static let sizeSQMTextColor         : UIColor = .gray
    static let priceTextColor           : UIColor = .black
    static let addressTextColor         : UIColor = .gray
    
    static let categoryTitleTextColor   : UIColor = .black
    static let viewMoreTextColor        : UIColor = .navigationBarColor()
}

struct UIConstants {
    static let searchBarHeight = 54.0
    static let bannerViewFrame : CGRect = CGRect(x: 0, y: 5, width: screenSize.width, height: 210)
}

struct ImageNames{
    static let PlaceHolder = "home_place_holder"
    static let Follow      = "follow"
    static let Followed    = "followed"
}
struct Strings{
    static let viewMore   = "View More >"
    static let ohMyHome   = "ohMyHome"
    static let postNow    = "Post Now"
    static let calculate  = "Calculate"
    static let openMap    = "Open Map"
    static let back       = "Back"
}

