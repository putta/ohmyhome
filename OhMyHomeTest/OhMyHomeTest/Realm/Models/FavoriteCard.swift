//
//  FavoriteCard.swift
//  OhMyHomeTest
//
//  Created by Chandra on 02/03/18.
//  Copyright © 2018 Chandra. All rights reserved.
//

import Foundation
import RealmSwift

class FavoriteCard: Object {
    @objc dynamic var _id = ""
    @objc dynamic var imageName = ""
}

