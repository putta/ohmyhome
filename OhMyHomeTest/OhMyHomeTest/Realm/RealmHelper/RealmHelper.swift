//
//  RealmHelper.swift
//  OhMyHomeTest
//
//  Created by Chandra on 02/03/18.
//  Copyright © 2018 Chandra. All rights reserved.
//

import Foundation
import RealmSwift

struct RealmHelper {
    static func save(object:Object)
    {
        let realm = try! Realm()
        try! realm.write{
            realm.add(object)
        }
    }
    
    static func delete(object:Object)
    {
        let realm = try! Realm()
        try! realm.write{
            realm.delete(object)
        }
    }
}

//MARK: Favorite operations
extension RealmHelper{
    
    static func favoriteItems(complection:@escaping ([FavoriteCard])->())
    {
        let realm = try! Realm()
        complection(realm.objects(FavoriteCard.self).map({$0}))
    }
    
    static func favoriteItemByID(_id: String, complection:@escaping (FavoriteCard?)->())
    {
        let realm = try! Realm()
        let realmObjects = realm.objects(FavoriteCard.self)
        let predicate = NSPredicate(format: "_id == %@", _id)
        if let item = realmObjects.filter(predicate).first
        {
            complection(item)
        }
        else{
            complection(nil)
        }
    }
    
    static func unfollowFavoriteCardWithID(_id:String)
    {
        self.favoriteItemByID(_id: _id, complection: { favoriteCard in
            if let card = favoriteCard {
                self.delete(object: card)
            }
        })
    }
}

