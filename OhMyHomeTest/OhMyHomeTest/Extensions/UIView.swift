//
//  UIView.swift
//  OhMyHomeTest
//
//  Created by Chandra on 24/02/18.
//  Copyright © 2018 Chandra. All rights reserved.
//

import UIKit

extension UIView
{
    func addContraintsWithFormat(_ format: String, views: UIView...) {
        var viewDict = [String: UIView]()
        
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewDict[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewDict))
    }
}

