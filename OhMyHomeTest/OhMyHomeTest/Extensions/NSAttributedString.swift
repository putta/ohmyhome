//
//  NSAttributedString.swift
//  OhMyHomeTest
//
//  Created by Chandra on 25/02/18.
//  Copyright © 2018 Chandra. All rights reserved.
//

import UIKit

extension String
{
    func attributedString(color:UIColor, font:UIFont) -> NSAttributedString {
        let attr = [
            NSAttributedStringKey.foregroundColor : color, NSAttributedStringKey.font:font
        ]
        return NSAttributedString(string: self, attributes: attr)
    }
}

