//
//  UIColor.swift
//  OhMyHomeTest
//
//  Created by Chandra on 24/02/18.
//  Copyright © 2018 Chandra. All rights reserved.
//

import UIKit

extension UIColor{
    static func rgb(_ r:CGFloat, _ g:CGFloat, _ b:CGFloat) -> UIColor {
        return UIColor(red:r/255.0,green:g/255.0,blue:b/255.0, alpha:1.0)
    }
    
    static func navigationBarColor()->UIColor{
        return UIColor.rgb(250.0, 118.0, 37.0)
    }
    
    static func statusBarColor() -> UIColor{
        return rgb(252.0,89.0,60.0)
    }
}

