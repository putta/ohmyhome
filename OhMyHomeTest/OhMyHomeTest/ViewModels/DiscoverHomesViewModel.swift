//
//  DiscoverHomesViewModel.swift
//  OhMyHomeTest
//
//  Created by Chandra on 24/02/18.
//  Copyright © 2018 Chandra. All rights reserved.
//

import UIKit

protocol DiscoverHomesViewModelDelegate : class {
    func bannersUpdated(banners:[Banner])
    func updateViewContentSignal(data:Any?)
    func specialBannerButtonTapped(view:SpecialBannerCellNode)
}

class DiscoverHomesViewModel: NSObject {
    weak var delegate : DiscoverHomesViewModelDelegate?
    var bannersArray : [Banner] = []
    var homeDataSourceArray : [HomeDataSource] = []
    
    //for holding cards array without sorting
    var rawDataHolderArray : [Any?] = []
    
    init(delegate:Any)
    {
        super.init()
        self.delegate = delegate as? DiscoverHomesViewModelDelegate
        getBannersFromServer()
        CardsType.allValues .forEach({getCards(cardType:$0)})
    }
    
    fileprivate func getBannersFromServer()
    {
        let url = URL(string:URLS.BannersUrl)!
        WebService().load(resource: parseBanners(withURL: url), serviceType: "Banner") { result, serviceTye  in
            
            switch result {
            case .success(let bannerModel):
                self.bannersArray = bannerModel.banners
                self.delegate?.bannersUpdated(banners: self.bannersArray)
            case .failure(let fail):
                print(fail.localizedDescription)
            }
        }
    }
    
    func getCards(cardType:CardsType) {
        let url = URL(string:(URLS.CardsUrl + cardType.rawValue))!
        WebService().load(resource: parseCards(withURL: url), serviceType:cardType) { result, cardType  in
            
            switch result {
            case .success(let cardsModel):
                if (cardsModel.cards.count > 0)
                {
                    var cardsArray : [Any] = cardsModel.cards
                    if let specialBanner = self.specialBanner(cardType: cardType as! CardsType)
                    {
                        if(cardsArray.count > 0)
                        {
                            cardsArray.insert(specialBanner, at: 0)
                        }
                    }
                    let homeDataSource = HomeDataSource(cardsArray:cardsArray, cardType:cardType as! CardsType)
                    self.rawDataHolderArray.append(homeDataSource)
                    //self.delegate?.updateViewContentSignal(data: self.homeDataSourceArray)
                }
                else{
                    self.rawDataHolderArray.append(nil)
                }
            case .failure(let fail):
                self.rawDataHolderArray.append(nil)
                print(fail.localizedDescription)
            }
            
            if(CardsType.allValues.count == self.rawDataHolderArray.count)
            {
                self.sortCardsListing()
            }
        }
    }
    
    func sortCardsListing()
    {
        let unSortedListingsArray : [HomeDataSource] = rawDataHolderArray.flatMap({ $0 }) as! [HomeDataSource]
        for type in CardsType.allValues
        {
            let value = unSortedListingsArray.filter({$0.cardType == type})
            if let card = value.first
            {
                homeDataSourceArray.append(card)
                self.delegate?.updateViewContentSignal(data: self.homeDataSourceArray)
            }
        }
    }
    
    func specialBanner(cardType:CardsType) -> Any? {
        
        let specialCardsArray = [CardsType.latest,CardsType.under, CardsType.openHouse]
        if(!specialCardsArray.contains(cardType))
        {
            return nil
        }
        
        let specialBanner = SpecialBannerCellNode()
        specialBanner.delegate = self
        switch cardType {
        case .latest:
            specialBanner.specialBannerType = .postMyProperty
        case .under:
            specialBanner.specialBannerType = .loanCalculator
        case .openHouse:
            specialBanner.specialBannerType = .openMap
        default:
            specialBanner.specialBannerType = .postMyProperty
        }
        return specialBanner
    }
    
    func bannerAtIndex(index:Int) -> Banner?
    {
        if(bannersArray.count > index)
        {
            return bannersArray[index]
        }
        return nil
    }
}

//MARK: SpecialBannerCellDelegate methods
extension DiscoverHomesViewModel: SpecialBannerCellDelegate
{
    func specialBannerButtonTapped(view: SpecialBannerCellNode) {
        self.delegate?.specialBannerButtonTapped(view: view)
    }
}

