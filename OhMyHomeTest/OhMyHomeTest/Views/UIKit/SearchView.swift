//
//  SearchView.swift
//  OhMyHomeTest
//
//  Created by Chandra on 01/03/18.
//  Copyright © 2018 Chandra. All rights reserved.
//

import UIKit

class SearchView: UIView {
    
    var searchBar : UISearchBar!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.navigationBarColor()
        searchBar = UISearchBar()
        searchBar.searchBarStyle = UISearchBarStyle.minimal
        searchBar.placeholder = " Search..."
        searchBar.isTranslucent = false
        searchBar.tintColor = UIColor.white
        searchBar.setPlaceholderTextColor(color: UIColor.white)
        searchBar.setSearchImageColor(color:UIColor.white)
        searchBar.setTextFieldClearButtonColor(color:UIColor.white)
        searchBar.setTextColor(color: UIColor.white)
        self.addSubview(searchBar)
        self.addContraintsWithFormat("H:|-10-[v0]-10-|", views: searchBar)
        self.addContraintsWithFormat("V:|-9-[v0(40)]-5-|", views: searchBar)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

