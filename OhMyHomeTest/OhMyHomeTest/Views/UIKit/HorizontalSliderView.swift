//
//  HorizontalSliderView.swift
//  OhMyHomeTest
//
//  Created by Chandra on 25/02/18.
//  Copyright © 2018 Chandra. All rights reserved.
//

import UIKit
import AsyncDisplayKit

enum CellType : Int
{
    case fullScreen = 0
    case splitScreen = 1
}

protocol HorizontalSliderViewDelegate : class {
    func horizontalSliderView(view:HorizontalSliderView, didSelectAt index:Int)
    func horizontalSliderView(view:HorizontalSliderView, viewMore sender:UIButton)
}

class HorizontalSliderView: UIView {
    
    weak var delegate : HorizontalSliderViewDelegate?
    var collectionNode : ASCollectionNode!
    var categoryNameLabel : UILabel = {
        let label = UILabel()
        label.font = Fonts.categoryTitleFont
        label.textColor = Colors.categoryTitleTextColor
        return label
    }()
    
    var viewMoreButton : UIButton = {
        let button = UIButton()
        button.setAttributedTitle(Strings.viewMore.attributedString(color: Colors.viewMoreTextColor, font: Fonts.viewMoreFont), for: .normal)
        button.addTarget(self, action:#selector(HorizontalSliderView.viewMoreTapped(sender:)), for: .touchUpInside)
        return button
    }()
    
    var categoryName : String = ""{
        didSet{
            categoryNameLabel.text = categoryName
        }
    }
    
    var loadbleCellType : CellType = CellType(rawValue: 0)! {
        didSet {
            if let layout  = collectionNode.collectionViewLayout as? UICollectionViewFlowLayout
            {
                switch loadbleCellType {
                case .fullScreen:
                    layout.itemSize.width = self.frame.size.width - DiscoverCellLayout.HorizontalCardPadding
                case .splitScreen:
                    layout.itemSize.width = self.frame.size.width/2 - DiscoverCellLayout.HorizontalCardPadding
                }
            }
        }
    }
    var dataSource : HomeDataSource? {
        didSet {
            collectionNode.reloadData()
        }
    }
    
    var cardSize : CGSize!
    
    init(frame: CGRect, cellType:CellType)
    {
        super.init(frame: frame)
        configureSlider(cellType:cellType)
    }
    
    func configureSlider(cellType:CellType)  {
        cardSize = frame.size
        let headerHeight  = DiscoverCellLayout.CardHeaderHeight
        self.addSubview(viewMoreButton)
        self.addSubview(categoryNameLabel)
        
        self.addContraintsWithFormat("H:|-5-[v0]-5-[v1(120)]-5-|", views: categoryNameLabel,viewMoreButton)
        self.addContraintsWithFormat("V:|[v0(\(headerHeight))]|", views: categoryNameLabel)
        self.addContraintsWithFormat("V:|[v0(\(headerHeight))]|", views: viewMoreButton)
        
        var layout: UICollectionViewFlowLayout!
        
        layout = UICollectionViewFlowLayout()
        layout.itemSize.width = self.frame.size.width - DiscoverCellLayout.HorizontalCardPadding
        //layout = CenterCellCollectionViewFlowLayout()
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        layout.minimumLineSpacing = 5
        layout.sectionInset = DiscoverCellLayout.flowLayoutSectionInsets
        
        collectionNode = ASCollectionNode(frame: CGRect(x: 0, y:headerHeight, width:cardSize.width, height:cardSize.height - headerHeight - 5), collectionViewLayout: layout)
        switch cellType {
        case .fullScreen:
            layout.itemSize = CGSize(width: cardSize.width - DiscoverCellLayout.HorizontalCardPadding, height: cardSize.height - headerHeight-5)
        case .splitScreen:
            layout.itemSize = CGSize(width: cardSize.width/2 - DiscoverCellLayout.HorizontalCardPadding, height: cardSize.height - headerHeight-5)
        }
        collectionNode.view.showsHorizontalScrollIndicator = false
        collectionNode.delegate = self
        collectionNode.dataSource = self
        self.addSubnode(collectionNode)
        collectionNode.reloadData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func viewMoreTapped(sender:UIButton)
    {
        self.delegate?.horizontalSliderView(view: self, viewMore: sender)
    }
}

extension HorizontalSliderView:ASCollectionDelegate,ASCollectionDataSource
{
    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        if let count = dataSource?.cardsArray.count
        {
            return count
        }
        return 0
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) ->ASCellNodeBlock {
        if let card = dataSource?.cardsArray[indexPath.item]{
            let nodeBlock: ASCellNodeBlock = {
                if card is Card
                {
                    return DiscoverCellNode(card:card as! Card ,size:self.cardSize)
                }
                else if card is ASCellNode
                {
                    return card as! ASCellNode
                }
                else{
                    return ASCellNode()
                }
            }
            return nodeBlock
        }
        else{
            let nodeBlock: ASCellNodeBlock = {
                return ASCellNode()
            }
            return nodeBlock
        }
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.horizontalSliderView(view: self, didSelectAt: indexPath.row)
    }
    
    private func debugSettings() {
        if(AppState.isDebug)
        {
            self.categoryNameLabel.backgroundColor = UIColor.orange
            self.viewMoreButton.backgroundColor = UIColor.green
            self.collectionNode.backgroundColor = UIColor.cyan
        }
        else
        {
            self.categoryNameLabel.backgroundColor = UIColor.clear
            self.viewMoreButton.backgroundColor = UIColor.clear
            self.collectionNode.backgroundColor = UIColor.clear
        }
    }
}


