//
//  BannerView.swift
//  OhMyHomeTest
//
//  Created by Chandra on 01/03/18.
//  Copyright © 2018 Chandra. All rights reserved.
//

import UIKit
import AsyncDisplayKit

protocol BannerViewDelegate : class {
    func bannerView(view:BannerView, didSelectAt index:Int)
}

class BannerView: UIView  {
    var pagingTimer = Timer()
    weak var delegate : BannerViewDelegate?
    private var pageControl = UIPageControl(frame: .zero)
    var collectionNode : ASCollectionNode!
    var dataSource : [Banner]!
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    init(frame:CGRect, dataSource : [Banner])
    {
        super.init(frame: frame)
        self.dataSource = dataSource
        configureSlider()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureSlider()
    {
        var layout: UICollectionViewFlowLayout!
        
        layout = CenterCellCollectionViewFlowLayout()
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        layout.minimumLineSpacing = 5
        layout.sectionInset = DiscoverCellLayout.flowLayoutSectionInsets
        collectionNode = ASCollectionNode(frame: self.frame, collectionViewLayout: layout)
        layout.itemSize = CGSize(width:screenSize.width-10, height:self.frame.size.height)
        collectionNode.view.showsHorizontalScrollIndicator = false
        collectionNode.delegate = self
        collectionNode.dataSource = self
        collectionNode.view.isPrefetchingEnabled = true
        self.addSubnode(collectionNode)
        collectionNode.reloadData()
        setupPageControl()
    }
    
    @objc func slideBanners(timer:Timer)
    {
        let row = timer.userInfo as! Int
        if(row == 0)
        {
            let transition = CATransition()
            transition.duration = 0.5
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromRight
            transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
            collectionNode.layer.add(transition, forKey: kCATransition)
            self.collectionNode.scrollToItem(at: IndexPath(row:row, section:0), at: .centeredHorizontally, animated: false)
        }
        else{
            self.collectionNode.scrollToItem(at: IndexPath(row:row, section:0), at: .centeredHorizontally, animated: true)
        }
        pageControl.currentPage = row
    }
    
    private func setupPageControl() {
        
        pageControl.numberOfPages = dataSource.count
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        pageControl.currentPageIndicatorTintColor = UIColor.orange
        pageControl.pageIndicatorTintColor = UIColor.white
        pageControl.currentPage = 0
        
        let view = self
        let leading = NSLayoutConstraint(item: pageControl, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: pageControl, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: pageControl, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        
        view.insertSubview(pageControl, at: 0)
        view.bringSubview(toFront: pageControl)
        view.addConstraints([leading, trailing, bottom])
    }
}

extension BannerView:ASCollectionDelegate,ASCollectionDataSource
{
    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        if let count = dataSource?.count
        {
            return count
        }
        return 0
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, nodeForItemAt indexPath: IndexPath) ->ASCellNode {
        
        if let banner = dataSource?[indexPath.row]
        {
            return BannerCellNode(banner:banner)
        }
        return ASCellNode()
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, willDisplayItemWith node: ASCellNode) {
        
        self.pagingTimer.invalidate()
        if var rowIndex = node.indexPath?.row
        {
            self.pageControl.currentPage = rowIndex
            let numberOfRecords = (self.dataSource?.count)! - 1
            if(rowIndex < numberOfRecords)
            {
                rowIndex += 1
            }
            else
            {
                rowIndex = 0
            }
            DispatchQueue.main.async {
                self.pagingTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.slideBanners(timer:)), userInfo: rowIndex, repeats: false)
            }
        }
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.bannerView(view: self, didSelectAt: indexPath.row)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        var insets = collectionNode.contentInset
        insets.left = 0
        insets.right = 0
        collectionNode.contentInset = insets
        print("\(collectionNode)")
        collectionNode.view.decelerationRate = UIScrollViewDecelerationRateFast
    }
}

