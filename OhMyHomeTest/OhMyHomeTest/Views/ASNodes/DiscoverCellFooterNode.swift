//
//  DiscoverCellFooterNode.swift
//  OhMyHomeTest
//
//  Created by Chandra on 25/02/18.
//  Copyright © 2018 Chandra. All rights reserved.
//
//"\u{1F4F7}"
import UIKit
import AsyncDisplayKit


class DiscoverCellFooterNode: ASDisplayNode {
    
    let priceNode : ASTextNode = {
        let label = ASTextNode()
        label.maximumNumberOfLines = 1
        label.truncationMode = .byTruncatingTail
        return label
    }()
    
    let addressNode : ASTextNode = {
        let label = ASTextNode()
        label.maximumNumberOfLines = 1
        label.truncationMode = .byTruncatingTail
        label.clipsToBounds = true
        return label
    }()
    
    let bedRoomCountNode : ASTextNode = {
        let label = ASTextNode()
        label.maximumNumberOfLines = 1
        label.truncationMode = .byTruncatingTail
        label.clipsToBounds = true
        return label
    }()
    
    let restRoomCountNode : ASTextNode = {
        let label = ASTextNode()
        label.maximumNumberOfLines = 1
        label.truncationMode = .byTruncatingTail
        label.clipsToBounds = true
        return label
    }()
    
    let areaCountNode : ASTextNode = {
        let label = ASTextNode()
        label.maximumNumberOfLines = 1
        label.truncationMode = .byTruncatingTail
        label.clipsToBounds = true
        return label
    }()
    
    let bedIcon : ASImageNode = {
        let image = ASImageNode()
        return image
    }()
    
    init?(card:Card)
    {
        super.init()
        priceNode.attributedText = card.attributedPriceString()
        addressNode.attributedText = card.attributedAddressString()
        addressNode.style.flexShrink = 1.0
        bedRoomCountNode.attributedText = card.attributedBedsCountString()
        restRoomCountNode.attributedText = card.attributedToiletsCountString()
        areaCountNode.attributedText = card.attributedSizeSQMString()
    }
    
    func footer() -> ASStackLayoutSpec {
        debugSettings()
        let priceSpacer = ASLayoutSpec()
        priceSpacer.style.flexGrow = 1.0
        let facilitiesSpacer = ASLayoutSpec()
        priceSpacer.style.flexGrow = 1.0
        
        let priceStackSpec = ASStackLayoutSpec(direction: .horizontal,
                                               spacing: DiscoverCellLayout.spacingFooterItems,
                                               justifyContent: .start,
                                               alignItems: .center,
                                               children: [priceNode,addressNode,priceSpacer])
        
        let facilitiesStackSpec = ASStackLayoutSpec(direction: .horizontal,
                                                    spacing: DiscoverCellLayout.spacingFooterItems,
                                                    justifyContent: .start,
                                                    alignItems: .start,
                                                    children: [bedRoomCountNode,restRoomCountNode,areaCountNode,facilitiesSpacer])
        
        let footerStackSpec = ASStackLayoutSpec(direction: .vertical,
                                                spacing: DiscoverCellLayout.spacingFooterItems,
                                                justifyContent: .start,
                                                alignItems: .start,
                                                children: [priceStackSpec,facilitiesStackSpec])
        return footerStackSpec
    }
    
    func debugSettings() {
        if(AppState.isDebug)
        {
            addressNode.backgroundColor = UIColor.purple
            priceNode.backgroundColor = UIColor.purple
            restRoomCountNode.backgroundColor = UIColor.purple
            bedRoomCountNode.backgroundColor = UIColor.purple
            areaCountNode.backgroundColor = UIColor.purple
        }
        else
        {
            addressNode.backgroundColor = UIColor.clear
            priceNode.backgroundColor = UIColor.clear
            restRoomCountNode.backgroundColor = UIColor.clear
            bedRoomCountNode.backgroundColor = UIColor.clear
            areaCountNode.backgroundColor = UIColor.clear
        }
    }
}

