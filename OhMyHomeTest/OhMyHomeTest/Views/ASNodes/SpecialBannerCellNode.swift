//
//  SpecialBannerCellNode.swift
//  OhMyHomeTest
//
//  Created by Chandra on 02/03/18.
//  Copyright © 2018 Chandra. All rights reserved.
//

import UIKit
import AsyncDisplayKit

enum SpecialBannerType : Int{
    case postMyProperty = 1
    case loanCalculator = 2
    case openMap = 3
}

protocol SpecialBannerCellDelegate : class {
    func specialBannerButtonTapped(view:SpecialBannerCellNode)
}

class SpecialBannerCellNode: ASCellNode {
    
    weak var delegate : SpecialBannerCellDelegate?
    let buttonNode : ASButtonNode = {
        let button = ASButtonNode()
        button.setAttributedTitle(Strings.postNow.attributedString(color: .cyan, font: Fonts.categoryTitleFont), for: .normal)
        button.backgroundColor = Colors.viewMoreTextColor
        button.contentEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
        button.cornerRadius = 5.0
        button.isUserInteractionEnabled = true
        return button
    }()
    
    var specialBannerType : SpecialBannerType = SpecialBannerType(rawValue: 1)!{
        didSet{
            switch specialBannerType {
            case .postMyProperty:
                backGroundNode.backgroundColor = .orange
                buttonNode.setAttributedTitle(Strings.postNow.attributedString(color: .white, font: Fonts.categoryTitleFont), for: .normal)
            case .loanCalculator:
                backGroundNode.backgroundColor = .yellow
                buttonNode.setAttributedTitle(Strings.calculate.attributedString(color: .white, font: Fonts.categoryTitleFont), for: .normal)
            case .openMap:
                backGroundNode.backgroundColor = .gray
                buttonNode.setAttributedTitle(Strings.openMap.attributedString(color: .white, font: Fonts.categoryTitleFont), for: .normal)
            }
        }
    }
    
    let backGroundNode : ASTextNode = {
        let label = ASTextNode()
        label.backgroundColor = .green
        label.maximumNumberOfLines = 1
        label.truncationMode = .byTruncatingTail
        label.isUserInteractionEnabled = true
        label.clipsToBounds = true
        return label
    }()
    
    override init() {
        super.init()
        self.automaticallyManagesSubnodes = true
        self.backgroundColor = UIColor.cyan
        buttonNode.addTarget(self, action: #selector(showMoreTapped(sender:)), forControlEvents: .touchUpInside)
    }
    
    @objc public func showMoreTapped(sender:UIButton)
    {
        self.delegate?.specialBannerButtonTapped(view: self)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let leadingSpacer = ASLayoutSpec()
        leadingSpacer.style.flexGrow = 1.0
        let trailingSpacer = ASLayoutSpec()
        trailingSpacer.style.flexGrow = 1.0
        
        let layoutStackSpec = ASStackLayoutSpec(direction: .horizontal,
                                                spacing: DiscoverCellLayout.spacingFooterItems,
                                                justifyContent: .start,
                                                alignItems: .start,
                                                children: [leadingSpacer,buttonNode,trailingSpacer])
        
        let textInsetSpec = ASInsetLayoutSpec(insets: DiscoverCellLayout.InsetForBotomCenteredButton, child: layoutStackSpec)
        return ASOverlayLayoutSpec(child: backGroundNode, overlay: textInsetSpec)
    }
}

