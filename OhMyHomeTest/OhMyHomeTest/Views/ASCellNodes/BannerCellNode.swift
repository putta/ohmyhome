//
//  BannerCellNode.swift
//  OhMyHomeTest
//
//  Created by Chandra on 01/03/18.
//  Copyright © 2018 Chandra. All rights reserved.
//

import UIKit
import AsyncDisplayKit

class BannerCellNode: ASCellNode {
    
    let photoImageNode: ASImageNode = {
        let imageNode = ASImageNode()
        imageNode.image = UIImage(named:ImageNames.PlaceHolder)
        imageNode.clipsToBounds = true
        imageNode.contentMode = .scaleAspectFill
        return imageNode
    }()
    
    init(banner:Banner)
    {
        super.init()
        WebService().loadImage(url: URL(string: (URLS.ImageUrl + banner.imageName)), placeHolder:ImageNames.PlaceHolder) { image in
            if let img = image{
                self.photoImageNode.image = img
            }
        }
        self.automaticallyManagesSubnodes = true
        self.backgroundColor = UIColor.white
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        return ASInsetLayoutSpec(insets: BannerCellLayout.InsetForBannerImage, child: photoImageNode)
    }
}

