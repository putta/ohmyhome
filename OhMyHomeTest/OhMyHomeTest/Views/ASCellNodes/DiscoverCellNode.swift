//
//  DiscoverCellNode.swift
//  OhMyHomeTest
//
//  Created by Chandra on 25/02/18.
//  Copyright © 2018 Chandra. All rights reserved.
//  (string: "https://ecowellness.com/wp-content/uploads/2017/04/property.jpg")


import UIKit
import AsyncDisplayKit

class DiscoverCellNode: ASCellNode {
    
    let photoImageNode: ASImageNode = {
        let imageNode = ASImageNode()
        imageNode.image = UIImage(named:ImageNames.PlaceHolder)
        imageNode.contentMode = .scaleAspectFill
        return imageNode
    }()
    
    var currentCard : Card!
    
    let followButtonNode: ASButtonNode = {
        let buttonNode = ASButtonNode()
        buttonNode.style.preferredSize = CGSize(width:44, height:44)
        buttonNode.setImage(UIImage(named:ImageNames.Follow), for: .normal)
        //        imageNode.image = UIImage(named:ImageNames.Follow)
        //        imageNode.style.preferredSize = CGSize(width:44, height:44)
        //        imageNode.contentMode = .scaleAspectFill
        return buttonNode
    }()
    
    
    var footerNode : DiscoverCellFooterNode?
    
    init(card: Card, size:CGSize) {
        super.init()
        currentCard = card
        WebService().loadImage(url: URL(string: (URLS.ImageUrl + card.imageName)), placeHolder:ImageNames.PlaceHolder) { image in
            if let img = image{
                self.photoImageNode.image = img
            }
        }
        photoImageNode.style.preferredSize = CGSize(width:size.width, height:200.0)
        followButtonNode.addTarget(self, action: #selector(followTapped(sender:)), forControlEvents: .touchUpInside)
        self.footerNode = DiscoverCellFooterNode(card:card)
        
        isFollowHome { (follow) in
            if(follow)
            {
                self.followButtonNode.setImage(UIImage(named:ImageNames.Followed), for: .normal)
            }
            else
            {
                self.followButtonNode.setImage(UIImage(named:ImageNames.Follow), for: .normal)
            }
        }
        self.automaticallyManagesSubnodes = true
    }
    
    @objc func followTapped(sender:UIButton)
    {
        isFollowHome { (follow) in
            if(follow)
            {
                self.followButtonNode.setImage(UIImage(named:ImageNames.Follow), for: .normal)
                RealmHelper.unfollowFavoriteCardWithID(_id: self.currentCard._id)
            }
            else
            {
                self.followButtonNode.setImage(UIImage(named:ImageNames.Followed), for: .normal)
                let favoriteCard = FavoriteCard()
                favoriteCard._id = self.currentCard._id
                favoriteCard.imageName = self.currentCard.imageName
                RealmHelper.save(object: favoriteCard)
            }
        }
    }
    
    func isFollowHome(completion:@escaping (Bool)->())
    {
        RealmHelper.favoriteItemByID(_id: currentCard._id, complection: { (favoriteCard) in
            if let _ = favoriteCard{
                completion(true)
            }
            else
            {
                completion(false)
            }
        })
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        
        let spacer = ASLayoutSpec()
        spacer.style.flexGrow = 1.0
        let verticalStack = ASStackLayoutSpec.vertical()
        let specs = [ASInsetLayoutSpec(insets: DiscoverCellLayout.InsetZero, child: photoImageNode), footerNode?.footer()]
        verticalStack.children = specs.flatMap({ $0 })
        
        let textInsetSpec = ASInsetLayoutSpec(insets:DiscoverCellLayout.InsetForFollowButton, child: followButtonNode)
        return ASOverlayLayoutSpec(child: verticalStack, overlay: textInsetSpec)
    }
}

