//
//  ModelMapper.swift
//  OhMyHomeTest
//
//  Created by Chandra on 28/02/18.
//  Copyright © 2018 Chandra. All rights reserved.
//

import Foundation

func parseBanners(withURL: URL) -> Resource<BannerDataModel> {
    
    let parse = Resource<BannerDataModel>(url: withURL, parseJSON: { jsonData in
        guard let json = jsonData as? JSONDictionary else { return .failure(.errorParsingJSON)  }
        guard let model = BannerDataModel(dictionary:json) else { return .failure(.errorParsingJSON) }
        return .success(model)
    })
    return parse
}

func parseCards(withURL: URL) -> Resource<CardsDataModel> {
    
    let parse = Resource<CardsDataModel>(url: withURL, parseJSON: { jsonData in
        guard let json = jsonData as? JSONDictionary else { return .failure(.errorParsingJSON)  }
        guard let model = CardsDataModel(dictionary:json) else { return .failure(.errorParsingJSON) }
        return .success(model)
    })
    return parse
}

