//
//  WebService.swift
//  OhMyHomeTest
//
//  Created by Chandra on 26/02/18.
//  Copyright © 2018 Chandra. All rights reserved.
//


import UIKit
import SDWebImage

final class WebService{
    
    final func configHeaders() -> [AnyHashable:Any]
    {
        return [
            "Accept": "application/json",
            "x-api-key": URLS.ApiKey
        ]
    }
    
    func load<T>(resource: Resource<T>, serviceType:Any, completion: @escaping (Result<T>, _ serviceType:Any) -> ()) {
        
        let config = URLSessionConfiguration.default
        config.httpAdditionalHeaders = configHeaders()
        let urlSession = URLSession(configuration: config)
        let myQuery = urlSession.dataTask(with:resource.url, completionHandler: {
            data, response, error -> Void in
            let result = self.checkForNetworkErrors(data, response, error)
            DispatchQueue.main.async {
                switch result {
                case .success(let data):
                    completion(resource.parse(data), serviceType)
                case .failure(let error):
                    completion(.failure(error), serviceType)
                }
            }
        })
        myQuery.resume()
    }
    
    func loadImage(url:URL?,placeHolder:String?, completion: @escaping (UIImage?)-> ()) {
        SDWebImageDownloader.shared().setValue(URLS.ApiKey, forHTTPHeaderField: "x-api-key")
        let manager = SDWebImageManager.shared()
        SDWebImageDownloader.shared().shouldDecompressImages = false
        SDImageCache.shared().config.shouldDecompressImages = false
        //        SDImageCache.shared().config.shouldCacheImagesInMemory = false
        manager.loadImage(with: url, options: SDWebImageOptions.scaleDownLargeImages, progress: nil) { (image, data, error, cacheType, bool, url) in
            if let img = image{
                completion(img)
            }
            else{
                completion(UIImage(named:placeHolder ?? ""))
            }
        }
    }
}

extension WebService {
    
    fileprivate func checkForNetworkErrors(_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Result<Data> {
        // Check for errors in responses.
        if let error = error {
            let nsError = error as NSError
            if nsError.domain == NSURLErrorDomain && (nsError.code == NSURLErrorNotConnectedToInternet || nsError.code == NSURLErrorTimedOut) {
                return .failure(.noInternetConnection)
            } else {
                return .failure(.returnedError(error))
            }
        }
        
        if let response = response as? HTTPURLResponse, response.statusCode <= 200 && response.statusCode >= 299 {
            return .failure((.invalidStatusCode("Request returned status code other than 2xx \(response)")))
        }
        
        guard let data = data else { return .failure(.dataReturnedNil) }
        
        return .success(data)
    }
}

struct Resource<A> {
    let url: URL
    let parse: (Data) -> Result<A>
}

extension Resource {
    
    init(url: URL, parseJSON: @escaping (Any) -> Result<A>) {
        self.url = url
        self.parse = { data    in
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data, options: [])
                return parseJSON(jsonData)
            } catch {
                fatalError("Error parsing data")
            }
        }
    }
}

enum Result<T> {
    case success(T)
    case failure(NetworkingErrors)
}

enum NetworkingErrors: Error {
    case errorParsingJSON
    case noInternetConnection
    case dataReturnedNil
    case returnedError(Error)
    case invalidStatusCode(String)
    case customError(String)
}

